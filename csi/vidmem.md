```
root@light-beagle:/usr/share# lsmod
Module                  Size  Used by
vvcam_flash_led       167936  0
vidmem                139264  0
thead_video            81920  0
vvcam_dec400           69632  0
vi_pre                282624  0
vvcam_dw200           229376  0
vvcam_soc             106496  0
vvcam_isp_ry          933888  0
vvcam_isp            1159168  0
vha_info               90112  0
xrp_hw_simple          94208  0
xrp                   565248  1 xrp_hw_simple
pvrsrvkm             8585216  17
xrp_hw_comm            20480  1 xrp_hw_simple
vc8000                589824  0
vvcam_sensor          163840  0
hantrodec             921600  0
vha                   835584  1 vha_info
img_mem               696320  1 vha
dwc3                 2584576  0
roles                  40960  1 dwc3
dwc3_thead             28672  0
thead_ddr_pmu         262144  0
fce                   208896  0
galcore              3182592  0
bm_csi                557056  0
bm_visys               36864  2 bm_csi,xrp_hw_simple
```


```
root@light-beagle:/usr/share# ls -lha vidmem/
drwxr-xr-x    3 root     root        4.0K Nov  2  2022 .
drwxr-xr-x   63 root     root        4.0K Nov  2  2022 ..
drwxr-xr-x    3 root     root        4.0K Nov  2  2022 test
root@light-beagle:/usr/share# ls -lha vidmem/test/bin/            
drwxr-xr-x    2 root     root        4.0K Nov  2  2022 .
drwxr-xr-x    3 root     root        4.0K Nov  2  2022 ..
-rwxr-xr-x    1 root     root       10.1K Nov  2  2022 vidmem_test
```

```
/usr/share/vidmem/test/bin/vidmem_test 
Allocated buffer 0 of type 0 at paddr 0x0d000000 vaddr 0x3feaadc000 size 3110400 fd 4
Imported fd 4: paddr 0x0d000000 vaddr 0x3feaadc000 size 3110400
Allocated buffer 1 of type 0 at paddr 0x0d400000 vaddr 0x3fea7e4000 size 3110400 fd 5
Imported fd 5: paddr 0x0d400000 vaddr 0x3fea7e4000 size 3110400
Allocated buffer 2 of type 0 at paddr 0x0d800000 vaddr 0x3fea4ec000 size 3110400 fd 6
Imported fd 6: paddr 0x0d800000 vaddr 0x3fea4ec000 size 3110400
Allocated buffer 0 of type 1 at paddr 0x0dbfa000 vaddr 0x3fea1f4000 size 3110400 fd 7
Imported fd 7: paddr 0x0dbfa000 vaddr 0x3fea1f4000 size 3110400
Allocated buffer 1 of type 1 at paddr 0x0c5d1000 vaddr 0x3fe9efc000 size 3110400 fd 8
Imported fd 8: paddr 0x0c5d1000 vaddr 0x3fe9efc000 size 3110400
Allocated buffer 2 of type 1 at paddr 0x0702c000 vaddr 0x3fe9c04000 size 3110400 fd 9
Imported fd 9: paddr 0x0702c000 vaddr 0x3fe9c04000 size 3110400
Allocated buffer 0 of type 2 at paddr 0x67300000 vaddr 0xc size 3110400 fd 10
Imported fd 10: paddr 0x67300000 vaddr 0xc size 3110400
Allocated buffer 1 of type 2 at paddr 0x67600000 vaddr 0xc size 3110400 fd 11
Imported fd 11: paddr 0x67600000 vaddr 0xc size 3110400
Allocated buffer 2 of type 2 at paddr 0x67c00000 vaddr 0xc size 3110400 fd 12
Imported fd 12: paddr 0x67c00000 vaddr 0xc size 3110400
Allocated buffer 0 of type 3 at paddr 0x10000000 vaddr 0x3fe990c000 size 3110400 fd 13
Imported fd 13: paddr 0x10000000 vaddr 0x3fe990c000 size 3110400
Allocated buffer 1 of type 3 at paddr 0x12c00000 vaddr 0x3fe9614000 size 3110400 fd 14
Imported fd 14: paddr 0x12c00000 vaddr 0x3fe9614000 size 3110400
Allocated buffer 2 of type 3 at paddr 0x14900000 vaddr 0x3fe931c000 size 3110400 fd 15
Imported fd 15: paddr 0x14900000 vaddr 0x3fe931c000 size 3110400
```
