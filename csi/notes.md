# Testing:

```
​export ISP_LOG_LEVEL=3
​cd /usr/share/vi/isp/test
​./camera_demo1 2 0 1 0 1920 1080 1 30 7
```



# Modules:

```
root@light-beagle:~# lsmod
Module                  Size  Used by
vvcam_flash_led       167936  0
vidmem                139264  0
thead_video            81920  0
vvcam_dec400           69632  0
vi_pre                282624  0
vvcam_dw200           229376  0
vvcam_soc             106496  0
vvcam_isp_ry          933888  0
vvcam_isp            1159168  0
xrp_hw_simple          94208  0
xrp                   565248  1 xrp_hw_simple
xrp_hw_comm            20480  1 xrp_hw_simple
vvcam_sensor          163840  0
pvrsrvkm             8585216  17
vc8000                589824  0
hantrodec             921600  0
vha_info               90112  0
vha                   835584  1 vha_info
img_mem               696320  1 vha
dwc3                 2584576  0
roles                  40960  1 dwc3
dwc3_thead             28672  0
galcore              3182592  0
thead_ddr_pmu         262144  0
fce                   208896  0
bm_csi                557056  0
bm_visys               36864  2 bm_csi,xrp_hw_simple
```

# vi-kernel.service

```
cat /lib/systemd/system/vi-kernel.service
[Unit]
Description=vi kernel driver init Service.

[Service]
Type=simple
ExecStart=sh /usr/share/vi/insmod.sh
ExecStop=sh /usr/share/vi/rmmod.sh
RemainAfterExit=y

[Install]
WantedBy=multi-user.target
```

```
cat /usr/share/vi/insmod.sh
#!/bin/sh
#insmod vvcam_i2c
modprobe vvcam_sensor
#insmod vvcam_csi
modprobe bm_visys
modprobe bm_csi
modprobe vvcam_isp
modprobe vvcam_isp_ry
modprobe vvcam_soc
modprobe vvcam_dw200
modprobe vi_pre
modprobe vvcam_dec400
modprobe thead_video
modprobe vidmem
modprobe vvcam_flash_led
```

```
cat /usr/share/vi/rmmod.sh
#!/bin/sh
rmmod vvcam_dw200
rmmod vvcam_soc
rmmod vvcam_isp
rmmod vvcam_isp_ry
rmmod vvcam_sensor
rmmod vvcam_csi
rmmod vvcam_i2c
rmmod vi_pre
rmmod vvcam_dec400
rmmod thead_video
rmmod vidmem
rmmod vvcam_flash_led
```

```
root@light-beagle:~# ls -lha /lib/modules/5.10.113-yocto-standard/extra/
drwxr-xr-x    4 root     root        4.0K Nov  2 14:53 .
drwxr-xr-x    4 root     root        4.0K Nov  2 14:54 ..
-rw-r--r--    1 root     root      530.2K Nov  2 14:50 bm_csi.ko
-rw-r--r--    1 root     root       32.9K Nov  2 14:50 bm_visys.ko
-rwxr-xr-x    1 root     root        1.2M Nov  2 14:50 driver_120_k.ko
-rwxr-xr-x    1 root     root      222.8K Nov  2 14:50 drm_nulldisp.ko
drwxr-xr-x    2 root     root        4.0K Nov  2 14:53 dsp
-rwxr-xr-x    1 root     root      224.7K Nov  2 14:50 fce.ko
-rw-r--r--    1 root     root      853.3K Nov  2 14:50 hantrodec.ko
-rw-r--r--    1 root     root      743.6K Nov  2 14:50 img_mem.ko
drwxr-xr-x    2 root     root        4.0K Nov  2 14:53 ivs
-rwxr-xr-x    1 root     root      600.7K Nov  2 14:50 lcik_main.ko
-rw-r--r--    1 root     root       27.4K Nov  2 14:50 memalloc.ko
-rwxr-xr-x    1 root     root        8.3M Nov  2 14:50 pvrsrvkm.ko
-rwxr-xr-x    1 root     root      331.0K Nov  2 14:50 tcrypt.ko
-rwxr-xr-x    1 root     root      220.3K Nov  2 14:50 testmgr_rambus.ko
-rw-r--r--    1 root     root      265.3K Nov  2 14:50 thead-ddr-pmu.ko
-rw-r--r--    1 root     root       91.0K Nov  2 14:50 thead_video.ko
-rwxr-xr-x    1 root     root      134.2K Nov  2 14:50 umdevxs_k.ko
-rw-r--r--    1 root     root      618.6K Nov  2 14:50 vc8000.ko
-rw-r--r--    1 root     root      885.8K Nov  2 14:50 vha.ko
-rw-r--r--    1 root     root       83.4K Nov  2 14:50 vha_info.ko
-rw-r--r--    1 root     root       16.8K Nov  2 14:50 vha_monitor.ko
-rw-r--r--    1 root     root      260.0K Nov  2 14:50 vi_pre.ko
-rw-r--r--    1 root     root      140.6K Nov  2 14:50 vidmem.ko
-rw-r--r--    1 root     root       65.4K Nov  2 14:50 vvcam_dec400.ko
-rw-r--r--    1 root     root      212.4K Nov  2 14:50 vvcam_dw200.ko
-rw-r--r--    1 root     root      171.1K Nov  2 14:50 vvcam_flash_led.ko
-rw-r--r--    1 root     root        1.1M Nov  2 14:50 vvcam_isp.ko
-rw-r--r--    1 root     root      899.0K Nov  2 14:50 vvcam_isp_ry.ko
-rw-r--r--    1 root     root      161.9K Nov  2 14:50 vvcam_sensor.ko
-rw-r--r--    1 root     root      108.0K Nov  2 14:50 vvcam_soc.ko
```

```
root@light-beagle:~# ls -lha /lib/modules/5.10.113-yocto-standard/extra/dsp/
drwxr-xr-x    2 root     root        4.0K Nov  2 14:53 .
drwxr-xr-x    4 root     root        4.0K Nov  2 14:53 ..
-rwxr-xr-x    1 root     root         423 Nov  2 14:50 insmod.sh
-rwxr-xr-x    1 root     root         652 Nov  2 14:50 insmod_load_fw_man.sh
-rwxr-xr-x    1 root     root         116 Nov  2 14:50 rmmod.sh
-rw-r--r--    1 root     root      587.2K Nov  2 14:50 xrp.ko
-rw-r--r--    1 root     root       15.6K Nov  2 14:50 xrp_hw_comm.ko
-rw-r--r--    1 root     root      101.9K Nov  2 14:50 xrp_hw_simple.ko
```

```
root@light-beagle:/usr/share/vi/isp/test# ls -lha
drwxr-xr-x    3 root     root        4.0K Nov  2 14:53 .
drwxr-xr-x    3 root     root        4.0K Nov  2 14:53 ..
-rwxr-xr-x    1 root     root       33.6K Nov  2 14:52 3aconfig.json
-rw-r--r--    1 root     root       33.7K Nov  2 14:52 3aconfig_GC02M1B.json
-rw-r--r--    1 root     root       33.6K Nov  2 14:52 3aconfig_GC5035.json
-rw-r--r--    1 root     root       33.6K Nov  2 14:52 3aconfig_IMX219.json
-rw-r--r--    1 root     root       33.6K Nov  2 14:52 3aconfig_SC132GS.json
-rw-r--r--    1 root     root       33.6K Nov  2 14:52 3aconfig_SC2310.json
-rwxr-xr-x    1 root     root       33.6K Nov  2 14:52 3aconfig_ry.json
-rw-r--r--    1 root     root      102.5K Nov  2 14:52 GC02M1B_1600x1200.xml
-rw-r--r--    1 root     root        2.0K Nov  2 14:52 GC02M1B_mipi1lane_1600x1200@30_mayi.txt
-rw-r--r--    1 root     root       80.9K Nov  2 14:52 GC5035.xml
-rw-r--r--    1 root     root       81.0K Nov  2 14:52 GC5035_1280x720.xml
-rw-r--r--    1 root     root       81.0K Nov  2 14:52 GC5035_1296x972.xml
-rw-r--r--    1 root     root       81.0K Nov  2 14:52 GC5035_1920x1080.xml
-rw-r--r--    1 root     root       95.6K Nov  2 14:52 GC5035_2592x1944.xml
-rw-r--r--    1 root     root       80.9K Nov  2 14:52 GC5035_640x480.xml
-rw-r--r--    1 root     root        1.7K Nov  2 14:52 GC5035_mipi2lane_1280x720@30_gc.txt
-rw-r--r--    1 root     root        2.5K Nov  2 14:52 GC5035_mipi2lane_1280x720@30_mayi.txt
-rw-r--r--    1 root     root        2.5K Nov  2 14:52 GC5035_mipi2lane_1280x720@60_bitland.txt
-rw-r--r--    1 root     root        2.6K Nov  2 14:52 GC5035_mipi2lane_1280x720@60_mayi.txt
-rw-r--r--    1 root     root        2.5K Nov  2 14:52 GC5035_mipi2lane_1296x972@30_bitland.txt
-rw-r--r--    1 root     root        2.5K Nov  2 14:52 GC5035_mipi2lane_1296x972@30_mayi.txt
-rw-r--r--    1 root     root        1.6K Nov  2 14:52 GC5035_mipi2lane_1920x1080@30_gc.txt
-rw-r--r--    1 root     root        2.5K Nov  2 14:52 GC5035_mipi2lane_2592x1922@30_bitland.txt
-rw-r--r--    1 root     root        2.5K Nov  2 14:52 GC5035_mipi2lane_2592x1922@30_mayi.txt
-rw-r--r--    1 root     root        1.6K Nov  2 14:52 GC5035_mipi2lane_2592x1944@30_gc.txt
-rw-r--r--    1 root     root        1.7K Nov  2 14:52 GC5035_mipi2lane_640x480@30_gc.txt
-rw-r--r--    1 root     root      102.5K Nov  2 14:52 IMX219_1640x1232.xml
-rw-r--r--    1 root     root      102.5K Nov  2 14:52 IMX219_1920x1080.xml
-rw-r--r--    1 root     root      102.5K Nov  2 14:52 IMX219_3280x2464.xml
-rw-r--r--    1 root     root      102.5K Nov  2 14:52 IMX219_640x480.xml
-rw-r--r--    1 root     root         707 Nov  2 14:52 IMX219_mipi4lane_1920x1080@30.txt
-rwxr-xr-x    1 root     root       27.2K Nov  2 14:52 IMX290.drv
-rwxr-xr-x    1 root     root       81.7K Nov  2 14:52 IMX290.xml
-rwxr-xr-x    1 root     root       81.7K Nov  2 14:52 IMX290_8M_02_1080p.xml
-rwxr-xr-x    1 root     root       81.7K Nov  2 14:52 IMX290_8M_02_720p.xml
-rwxr-xr-x    1 root     root      111.4K Nov  2 14:52 IMX290_fisheye.xml
-rwxr-xr-x    1 root     root        1.1K Nov  2 14:52 IMX290_mipi4lane_1080p_init.txt
-rwxr-xr-x    1 root     root       81.1K Nov  2 14:52 IMX290_pentax_04.xml
-rwxr-xr-x    1 root     root       81.1K Nov  2 14:52 IMX290_pentaxcombined_01.xml
-rwxr-xr-x    1 root     root       26.8K Nov  2 14:52 IMX334.drv
-rwxr-xr-x    1 root     root       81.7K Nov  2 14:52 IMX334.xml
lrwxrwxrwx    1 root     root          10 Nov  2 14:53 IMX334_8M_02_1080p.drv -> IMX334.drv
-rwxr-xr-x    1 root     root       81.7K Nov  2 14:52 IMX334_8M_02_1080p.xml
lrwxrwxrwx    1 root     root          10 Nov  2 14:53 IMX334_fisheye.drv -> IMX334.drv
-rwxr-xr-x    1 root     root      111.4K Nov  2 14:52 IMX334_fisheye.xml
-rw-r--r--    1 root     root        1.1K Nov  2 14:52 IMX334_mipi4lane_3840_2160_raw12_800mbps_3dol_init.txt
-rw-r--r--    1 root     root        1.9K Nov  2 14:52 IMX334_mipi4lane_3840_2160_raw12_800mbps_init.txt
drwxr-xr-x    3 root     root        4.0K Nov  2 14:53 ISP8000L_V2008
-rw-r--r--    1 root     root       95.5K Nov  2 14:52 OV12870.xml
-rw-r--r--    1 root     root       81.0K Nov  2 14:52 OV12870_1920x1080.xml
-rw-r--r--    1 root     root       80.9K Nov  2 14:52 OV12870_640x480.xml
-rwxr-xr-x    1 root     root       14.3K Nov  2 14:52 OV12870_mipi4lane_1920x1080_1200_30f.txt
-rwxr-xr-x    1 root     root       14.3K Nov  2 14:52 OV12870_mipi4lane_4096X3072_1200_30f_init.txt
-rwxr-xr-x    1 root     root       14.3K Nov  2 14:52 OV12870_mipi4lane_640x480_init.txt
-rwxr-xr-x    1 root     root       81.7K Nov  2 14:52 OV2775.xml
-rwxr-xr-x    1 root     root       81.7K Nov  2 14:52 OV2775_8M_02_1080p.xml
-rwxr-xr-x    1 root     root       81.7K Nov  2 14:52 OV2775_8M_02_720p.xml
-rwxr-xr-x    1 root     root      111.4K Nov  2 14:52 OV2775_fisheye.xml
-rwxr-xr-x    1 root     root       21.9K Nov  2 14:52 OV2775_mipi4lane_1080p_2dol_init.txt
-rwxr-xr-x    1 root     root       21.9K Nov  2 14:52 OV2775_mipi4lane_1080p_3dol_init.txt
-rw-r--r--    1 root     root       21.2K Nov  2 14:52 OV2775_mipi4lane_1920x1080_raw12_init.txt
-rwxr-xr-x    1 root     root       22.0K Nov  2 14:52 OV2775_mipi4lane_vga_init.txt
-rwxr-xr-x    1 root     root       81.1K Nov  2 14:52 OV2775_pentax_04.xml
-rwxr-xr-x    1 root     root       81.1K Nov  2 14:52 OV2775_pentaxcombined_01.xml
-rwxr-xr-x    1 root     root       95.5K Nov  2 14:52 OV5693.xml
-rwxr-xr-x    1 root     root        3.0K Nov  2 14:52 OV5693_mipi2lane_640x480_init.txt
-rw-r--r--    1 root     root       95.5K Nov  2 14:52 SC132GS.xml
-rw-r--r--    1 root     root       95.6K Nov  2 14:52 SC132GS_1080x1280.xml
-rw-r--r--    1 root     root        1.2K Nov  2 14:52 SC132GS_mipi2lane_1080x1280_init.txt
-rw-r--r--    1 root     root        1.2K Nov  2 14:52 SC132GS_mipi2lane_1080x1280_master_init.txt
-rw-r--r--    1 root     root        1.3K Nov  2 14:52 SC132GS_mipi2lane_1080x1280_slave_init.txt
-rw-r--r--    1 root     root        1.4K Nov  2 14:52 SC132GS_mipi2lane_960x1280_init.txt
-rw-r--r--    1 root     root        1.5K Nov  2 14:52 SC132GS_mipi2lane_960x1280_master_init.txt
-rw-r--r--    1 root     root        1.5K Nov  2 14:52 SC132GS_mipi2lane_960x1280_slave_init.txt
-rw-r--r--    1 root     root      102.6K Nov  2 14:52 SC2310_1920x1080.xml
-rw-r--r--    1 root     root      104.1K Nov  2 14:52 SC2310_1920x1088.xml
-rw-r--r--    1 root     root        2.0K Nov  2 14:52 SC2310_mipi2lane_1920x1080_raw10_30fps_init.txt
-rw-r--r--    1 root     root        1.9K Nov  2 14:52 SC2310_mipi2lane_1920x1088_raw12_30fps_init.txt
-rw-r--r--    1 root     root        2.0K Nov  2 14:52 SC2310_mipi2lane_640x480_raw12_30fps_init.txt
-rwxr-xr-x    1 root     root         520 Nov  2 14:52 Sensor0_Entry.cfg
-rwxr-xr-x    1 root     root         282 Nov  2 14:52 Sensor1_Entry.cfg
-rwxr-xr-x    1 root     root       30.4K Nov  2 14:52 camera_demo1
-rwxr-xr-x    1 root     root       30.5K Nov  2 14:52 camera_demo2
-rwxr-xr-x    1 root     root       30.4K Nov  2 14:52 camera_demo4
-rwxr-xr-x    1 root     root       30.4K Nov  2 14:52 camera_demo5
-rwxr-xr-x    1 root     root       30.4K Nov  2 14:52 camera_demo6
-rwxr-xr-x    1 root     root       30.4K Nov  2 14:52 camera_demo7
-rwxr-xr-x    1 root     root       34.4K Nov  2 14:52 camera_demo_dual_ir
-rwxr-xr-x    1 root     root       30.4K Nov  2 14:52 camera_ir_demo
-rwxr-xr-x    1 root     root      179.0K Nov  2 14:52 camera_test
-rwxr-xr-x    1 root     root       48.1K Nov  2 14:52 camera_test1
-rwxr-xr-x    1 root     root       40.4K Nov  2 14:52 camera_test_mt
-rw-r--r--    1 root     root         746 Nov  2 14:52 dwconfig.json
-rw-r--r--    1 root     root         742 Nov  2 14:52 dwconfig_SC2310.json
-rwxr-xr-x    1 root     root      183.6K Nov  2 14:52 isp_test
-rwxr-xr-x    1 root     root       27.1K Nov  2 14:52 ov12870.drv
-rwxr-xr-x    1 root     root       31.1K Nov  2 14:52 ov2775.drv
-rwxr-xr-x    1 root     root       27.2K Nov  2 14:52 ov5693.drv
-rwxr-xr-x    1 root     root       35.6K Nov  2 14:52 test_cam_engine
-rw-r--r--    1 root     root         112 Nov  2 14:52 tuning-pipeline.json
-rwxr-xr-x    1 root     root         508 Nov  2 14:52 vgbmcase.sh
-rwxr-xr-x    1 root     root         173 Nov  2 14:52 vgswcase.sh
-rwxr-xr-x    1 root     root        4.1K Nov  2 14:52 video_property.yaml
```
